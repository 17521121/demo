var _ = require('lodash');
module.exports = User => {
  User.prototype.tenapi = (req, res, cb) => {
    return new Promise(async (resolve, reject) => {
      try {
        let app = User.app;
        let todo = app.models.todo;

        let user = await User.create({ ten: "demoUser" }); // phải xác định được user trước khi thêm công việc
        let x = await user.todos.create({ noidung: "Edited" }); //user là trong biến, khác trong mongoose
        resolve(x);
      } catch (error) {
        reject(error);
      }
    })
  }
}