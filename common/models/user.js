'use strict';

module.exports = function (User) {
  require('../classes/user')(User);


  User.tenapi = async (req, res, cb) => {
    try {
      let user = new User();
      return await user.tenapi(req, res);
    } catch (err) {
      cb(err);
    }
  }

  User.remoteMethod('tenapi', {
    description: 'Tạo api demo',
    http: { path: '/tenapi', verb: 'get' },
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'res', type: 'object', http: { source: 'res' } },
    ],
    returns: { arg: 'data', type: ['string'], root: true }
  });
};
